Import-Module Az

function elapsedTime {
    $CurrentTime = $(get-date)
    $elapsedTime = $CurrentTime - $StartTime
    $elapsedTime = [math]::Round($elapsedTime.TotalSeconds,2)
    Write-Host "Elapsed time in seconds: " $elapsedTime -BackgroundColor Blue
}
#Captures start time for script elapsed time measurement
$StartTime = $(get-date)

$resourceGroup = "DisposableLab"
$VMNames =  Get-AzVM -ResourceGroupName $resourceGroup #"WebappWin1", "WebappWin2"
$NICs = Get-AzNetworkInterface -ResourceGroupName $resourceGroup #"WebappWin1", "WebappWin2"
$LBs = Get-AzLoadBalancer -ResourceGroupName $resourceGroup  #"WebAppWinLB"
$IPs = Get-AzPublicIPAddress -ResourceGroupName $resourceGroup  #"LB1PublicIP", "WebappWin1PublicIP", "WebappWin2PublicIP"
$disks = Get-AzDisk -ResourceGroupName $resourceGroup

elapsedTime

foreach ($VMName in $VMNames.Name){
    Write-Host "Deleting $($VMName)"
    Remove-AzVM -ResourceGroupName $resourceGroup -Name $VMName -Force
    Write-Host "Deleted $($VMName)"
    elapsedTime
}



foreach ($LB in $LBs.Name){
    Write-Host "Deleting Load Balancer $($LB)"
    Remove-AzLoadBalancer -ResourceGroupName $resourceGroup -Name $LB -Force
    Write-Host "Deleted $($LB)"
    elapsedTime
}


foreach ($NIC in $NICs.Name){
    Write-Host "Deleting NIC $($NIC)"
    Remove-AzNetworkInterface -Name $NIC -ResourceGroup $resourceGroup -Force
    Write-Host "Deleted NIC $($NIC)"
    elapsedTime
}


foreach ($IP in $IPs.Name){
    Write-Host "Deleting IP $($IP)"
    Remove-AzPublicIpAddress -Name $IP -ResourceGroup $resourceGroup -Force
    Write-Host "Deleted IP $($IP)"
    elapsedTime
}


foreach ($disk in $disks.Name){
    Write-Host "Deleting Disk $($disk)"
    Remove-AzDisk -Name $disk -ResourceGroupName $resourceGroup -Force
    Write-Host "Deleted Disk $($disk)"
    elapsedTime
}

Write-Host "Script Completed" -BackgroundColor Blue
elapsedTime